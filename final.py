# coding: utf8

'''
Mini-projet "Choipeaux" première partie avec
les réponses qui s'affichent dans la console.

Auteurs: Clémence ABRASSART, Andrea BASSI, Sacha BECOTE

Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/
VERSION FINALE
07/03/2022

'''
import csv
from math import*

with open("U:/Documents/103/NSI/notebook/P - Fil rouge Harry Potter/nsi-master-Projets-3 - Projet Choixpeau magique/Projets/3 - Projet Choixpeau magique/Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_perso = [{key: value for key, value in element.items()} for element
                   in reader]

with open("U:/Documents/103/NSI/notebook/P - Fil rouge Harry Potter/nsi-master-Projets-3 - Projet Choixpeau magique/Projets/3 - Projet Choixpeau magique/Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_carac = [{key: value for key, value in element.items()} for element
                   in reader]

for element in table_carac:
    element['Courage'] = int(element['Courage'])
    element['Ambition'] = int(element['Ambition'])
    element['Intelligence'] = int(element['Intelligence'])
    element['Good'] = int(element['Good'])
    for perso in table_perso:
        if element['Name'] == perso['Name']:
            element['House'] = perso['House']


def distance(eleve1, eleve2):
    '''
    Calcule la distance entre deux élèves.
    Entrée: deux dictionnaires
    Sortie: un flottant
    '''
    return sqrt((eleve1['Good'] - eleve2['Good'])**2 + (eleve1['Courage'] -
                eleve2['Courage'])**2 + (eleve1['Ambition'] -
                eleve2['Ambition'])**2 + (eleve1['Intelligence'] -
                eleve2['Intelligence'])**2)


def combinaisons(table, eleve):
    '''
    Créé une liste des distances avec cet élève.
    Entrée: une table et un dictionnaire
    Sortie: une liste de liste (le personnage et sa distance à l'élève)
    '''
    liste_dist = []
    for element in table:
        liste_dist.append([element, distance(element, eleve)])
    return liste_dist


def plus_proches_voisins(k, liste):
    '''
    Détermine les k plus proches voisins
    Entrée: un entier, une table
    Sortie: une table
    '''
    voisins = []
    liste.sort(key=lambda x: x[1])
    for i in range(k):
        voisins.append([liste[i][0]['Name'], liste[i][0]['House']])
    return voisins


def maison(liste_voisins):
    '''
    Détermine la maison du nouvel élève
    Entrée: une table
    Sortie: une chaine de charactères
    '''
    maisons = {"Gryffindor": 0, "Slytherin": 0, "Ravenclaw": 0,
               "Hufflepuff": 0}
    for perso in liste_voisins:
        maisons[perso[1]] += 1
    la_maison = list(maisons.items())
    la_maison.sort(key=lambda x: x[1], reverse=True)
    return la_maison[0][0]


def affichage(eleve, table, k):
    '''
    Affiche les voisins et la maison de l'élève
    Entrée: un dictionnaire(caractéristiques de l'élève), une table(tous les
    persos avec leurs caractéristiques), un entier
    '''
    ses_voisins = plus_proches_voisins(k, combinaisons(table, eleve))
    sa_maison = maison(ses_voisins)
    print(f"Il(elle) a comme voisins {ses_voisins}\n"
          f"Il(elle) ira donc dans {sa_maison}\n ")


test1 = {'Good': 9, 'Courage': 9, 'Ambition': 2, 'Intelligence': 8}
test2 = {'Good': 7, 'Courage': 6, 'Ambition': 7, 'Intelligence': 9}
test3 = {'Good': 3, 'Courage': 3, 'Ambition': 8, 'Intelligence': 6}
test4 = {'Good': 8, 'Courage': 2, 'Ambition': 3, 'Intelligence': 8}
test5 = {'Good': 8, 'Courage': 3, 'Ambition': 4, 'Intelligence': 8}

print('test1')
affichage(test1, table_carac, 5)
print('test2')
affichage(test2, table_carac, 5)
print('test3')
affichage(test3, table_carac, 5)
print('test4')
affichage(test4, table_carac, 5)
print('test5')
affichage(test5, table_carac, 5)

print("Pour votre personnage")
courage = int(input('Le courage: '))
ambition = int(input("L'ambition: "))
intel = int(input("L'intelligence: "))
good = int(input("Le bien: "))
nb_voisins = int(input('Le nombre de voisins auquel vous voulez le comparer : '))
print()
nouv_eleve = {'Good': good, 'Courage': courage, 'Ambition': ambition,
              'Intelligence': intel}
affichage(nouv_eleve, table_carac, nb_voisins)

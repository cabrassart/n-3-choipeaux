# coding: utf8

'''
Mini-projet "Choipeaux" première partie avec
les réponses qui s'affichent dans la console.

Auteurs: Clémence ABRASSART, Andrea BASSI

Licence (CC BY-NC-SA 3.0 FR)
https://creativecommons.org/licenses/by-nc-sa/3.0/fr/
VERSION FINALE
24/03/2021

'''
import csv

with open("U:/Documents/103/NSI/notebook/P - Fil rouge Harry Potter/nsi-master-Projets-3 - Projet Choixpeau magique/Projets/3 - Projet Choixpeau magique/Characters.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_perso = [{key : value for key, value in element.items()} for element in reader]

with open("U:/Documents/103/NSI/notebook/P - Fil rouge Harry Potter/nsi-master-Projets-3 - Projet Choixpeau magique/Projets/3 - Projet Choixpeau magique/Caracteristiques_des_persos.csv", mode='r', encoding='utf-8') as f:
    reader = csv.DictReader(f, delimiter=';')
    table_carac = [{key : value for key, value in element.items()} for element in reader]

for element in table_carac:
    for perso in table_perso:
        if element['Name'] == perso['Name']:
            element['House'] = perso['House']


from math import*

courage = int(input('Le courage: '))
ambition = int(input("L'ambition: "))
intel = int(input("L'intelligence: "))
good = int(input("Le bien: "))

nouv_eleve = {'Good': good, 'Courage': courage, 'Ambition': ambition, 'Intelligence': intel}

def distance(eleve1, eleve2):
    '''
    calcule la distance entre deux élèves.
    Entrée: deux dictionnaires
    Sortie: un flottant
    '''
    return sqrt((eleve1['Good'] - eleve2['Good'])**2 + (eleve1['Courage'] - eleve2['Courage'])**2 + (eleve1['Ambition'] - eleve2['Ambition'])**2 + (eleve1['Intelligence'] - eleve2['Intelligence'])**2 )


def combinaisons(table, eleve):
    '''
    Créé une liste des distances avec cet élève
    Entrée: une table et un dictionnaire
    Sortie: une liste avec les combinaisons
    '''
    combin = []
    for element in table:
        combin.append([element, eleve, distance(element, eleve)])
    return combin

def plus_proches_voisins(k, liste):
    '''
    détermine les k plus proches voisins
    Entrée: un entier, une table
    Sortie: une table
    '''
    voisins = []
    liste.sort(key=lambda x: x[3])
    for i in range(k):
        voisins.append(liste[k][0])
    return voisins


def maison(liste_voisins):
    '''
    détermine la maison du nouvel élève
    Entrée: une table
    Sortie: une chaine de charactères
    '''
    maisons = [["Gryffindor", 0], ["Slytherin", 0], ["Ravenclaw", 0],
           ["Hufflepuff", 0]]
    for perso in liste_voisins:
        occurence[perso[1]] += 1
    maisons.sort(key=lambda x: x[1])
    return maisons[0][0]


def affichage(eleve, table, k):
    ses_voisins =  plus_proches_voisins(5, combinaisons(table, eleve))
    sa_maison = maison(ses_voisins)
    print(f"Il(elle) a comme voisins {ses_voisins} il(elle) ira donc dans {sa_maison}")




test1 = {'Good': 9, 'Courage': 9, 'Ambition': 2, 'Intelligence': 8}
test2 = {'Good': 7, 'Courage': 6, 'Ambition': 7, 'Intelligence': 9}
test3 = {'Good': 3, 'Courage': 3, 'Ambition': 8, 'Intelligence': 6}
test4 = {'Good': 8, 'Courage': 2, 'Ambition': 3, 'Intelligence': 8}
test5 = {'Good': 8, 'Courage': 3, 'Ambition': 4, 'Intelligence': 8}

affichage(test1, table_carac, 5)
affichage(test2, table_carac, 5)
affichage(test3, table_carac, 5)
affichage(test4, table_carac, 5)
affichage(nouv_eleve, table_carac, 5)